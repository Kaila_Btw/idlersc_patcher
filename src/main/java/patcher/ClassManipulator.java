package patcher;

import org.objectweb.asm.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

public class ClassManipulator {
    private static String workAreaPath = Main.getWorkAreaPath();

    public static boolean patchClass(String absoluteClassPath) {
        //orsc/ORSCApplet.java:	public void draw()
        System.out.println("Beginning class patching process");
        final String classPath = workAreaPath + absoluteClassPath;

            try {
            ClassWriter writer = new ClassWriter(0);
            ClassVisitor visitor = new ClassVisitor(Opcodes.ASM4, writer) {}; //'visitor' will forward all events to 'writer'
            ClassReader reader = new ClassReader(new FileInputStream(new File(classPath)));

            reader.accept(new MasterAdapter(visitor), 0); //ties the visitor to our reader
            System.out.println("Returning patched class file to be saved");
            return saveClassFile(writer.toByteArray(), classPath);
        }
        catch(Exception e) {
            System.out.println("EXCEPTION");
            e.printStackTrace();
            return false;
        }
    }

    private static boolean saveClassFile(byte[] data, String classPath) {
        System.out.println("Save class file method() started");
            try (FileOutputStream stream = new FileOutputStream(classPath)) {
                stream.write(data);
            System.out.println("Done saving patched class file");
                return true;
            } catch (Exception e) {
            System.out.println("EXCEPTION");
                e.printStackTrace();
                return false;
            }
    }
}